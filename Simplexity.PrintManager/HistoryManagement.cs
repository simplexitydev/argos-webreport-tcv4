﻿using System;
using Infrastructure.Crosscutting.EF;
using Simplexity.PrintManager.BusinessRules.DTOs;

namespace Simplexity.PrintManager.BusinessRules
{
    public static class HistoryManagement
    {
          #region HistoryManagement
        public static bool sendToHistory(adReportTypes report, ReportDTO reportDTO, UserDTO userDTO)
        {
            if (report.RptCreateHistory == Constants.Operation.True)
            {
                var addReportHistory = new adReportHistory
                                           {
                                               RphSourceId = reportDTO.SourceId,
                                               RphSourceCode = reportDTO.SourceCode,
                                               RphDate = DateTime.Now,
                                               RphReportType_RptCode = reportDTO.ReportCode,
                                               RphAction = 0,
                                               RphTimes = 0,
                                               RphUser = userDTO.Code
                                           };
                if (InsertHistory(addReportHistory) < 1)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        private static int InsertHistory(adReportHistory newItem)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                bd.adReportHistory.Add(newItem);
                return  bd.SaveChanges();
            }
        }
    }
}
