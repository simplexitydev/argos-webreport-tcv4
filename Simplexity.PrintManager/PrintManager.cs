﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Infrastructure.Crosscutting;
using Infrastructure.Crosscutting.EF;
using Infrastructure.Crosscutting.Reports;
using Simplexity.PrintManager.BusinessRules.Constants;
using Simplexity.PrintManager.BusinessRules.DTOs;
using Simplexity.PrintManager.BusinessRules.Resources;
using Simplexity.TC.TMS.Infrastructure.Crosscutting;

namespace Simplexity.PrintManager.BusinessRules
{
    public class PrintManager
    {
        #region declaration

        //private IRSPrintManager _RSPrintManager;
        private adReportTypes report;

        #endregion

        /// <summary>
        /// Allow print the corresponding report parameter passing
        /// </summary>
        /// <param name="ReportDTO">string reportCode, int sourceId, string sourceCode, string reportParams</param>
        /// <param name="UserDTO">string userCode List<string/> userCode</param>
        /// <returns>bool</returns>
        public MessageDTO PrintReport(ReportDTO reportDTO, string UserCode, string Sudcode, string Path)
        {
            var messageDTO = new MessageDTO() { TypeEnum = MessagesConstants.Types.Information };
            string result = string.Empty;
            GetReport(reportDTO.ReportCode);

            //_RSPrintManager = new RSPrintManager();

            var userDTO = GetUserDto(UserCode, Sudcode);


            #region  canBePrinted
            string canBePrinted = CanBePrinted(reportDTO.ReportCode, userDTO.Roles, userDTO.Code, reportDTO.SourceId,
                                               reportDTO.SourceCode);

            if (canBePrinted != string.Empty)
            {
                messageDTO.TypeEnum = MessagesConstants.Types.Warning;
                messageDTO.Message = string.Format(Messages.Not_allowed_to_print, "Report");
                return messageDTO;
            }
            #endregion


            #region  Get Report, if not found throw invalidoperationexeption
            GetReport(reportDTO.ReportCode);
            #endregion

            #region  Set ReportExecutionService NetworkCredential if Any
            NetworkCredential serviceCredential = null;
            adReportServers reportServer = GetReportServers(report.RptReportServer_RpsCode);

            if (reportServer != null)
            {
                serviceCredential = GetServiceCredential(reportServer);

                // string reportPath = string.Format("/{0}/{1}", reportServer.RpsPath, report.RptReportName);
                string reportPath = string.Format("/{0}/{1}", Path, report.RptReportName);


                #region  Get valid printer name
                adPrinters reportPrinter = GetReportPrinter(userDTO.Subsidiary, userDTO.Code, userDTO.Roles, report.RptCode);
                if (reportPrinter == null)
                {
                    messageDTO.TypeEnum = MessagesConstants.Types.Error;
                    messageDTO.Message = string.Format(Messages.error_CannotGetValidPrinter, report.RptCode, userDTO.Company, userDTO.Subsidiary, userDTO.Code);
                    return messageDTO;
                }

                #endregion

                #region Print

                List<Parameter> ssrsParameters = reportDTO.Params.Select(parameterDTO => new Parameter { Name = parameterDTO.Name, Value = parameterDTO.Value }).ToList();

                string printeractive = !string.IsNullOrWhiteSpace(reportPrinter.PriName) ? reportPrinter.PriName : (!string.IsNullOrWhiteSpace(reportPrinter.PriAddress) ? reportPrinter.PriAddress : string.Empty);
                //bool result = _RSPrintManager.PrintSSRSReport(reportPath, ssrsParameters, -1, -1, printeractive);

                var printWithReportingService = new PrintWithReportingService();
                result = printWithReportingService.PrintToPrint(reportPath, ssrsParameters, printeractive, serviceCredential, reportServer.RpsServiceUrl);
                if (result == string.Empty)
                {
                    messageDTO.Message = "Impresión satisfactoria";

                    #region send to history

                        HistoryManagement.sendToHistory(report, reportDTO, userDTO);

                    #endregion
                }
                else
                {
                    messageDTO.TypeEnum = MessagesConstants.Types.Error;
                    messageDTO.Message = "Error en la Impresión...,   " + result;
                }

                return messageDTO;
                #endregion
            }
            else
            {
                messageDTO.TypeEnum = MessagesConstants.Types.Warning;
                messageDTO.Message = string.Format(Messages.No_report_server, "Report");
                return messageDTO;
            }

            #endregion
        }

        #region get datos
        public string GetCompany(string Sudcode)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                if (!string.IsNullOrEmpty(Sudcode))
                {
                    adSubsidiaries company = bd.adSubsidiaries.FirstOrDefault(p => p.SubCode == Sudcode);
                    if (company != null)
                    {
                        return company.Sub_ComCode ?? string.Empty;
                    }
                }
                return string.Empty;
            }
        }

        private UserDTO GetUserDto(string userCode, string Sudcode)
        {
            var userDTO = new UserDTO();
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                var useritem = bd.adUsers.FirstOrDefault(p => p.UsrCode == userCode);
                if (useritem != null)
                {
                    userDTO.Code = useritem.UsrCode ?? string.Empty;
                    userDTO.Company = GetCompany(Sudcode);
                    userDTO.Subsidiary = Sudcode ?? string.Empty;
                    userDTO.Roles = null;//new List<string>();
                    //var listUserRol = bd.adUsersRole.Where(p => p.Uro_UsrCode == userCode).ToList();
                    //foreach (var itemRol in listUserRol)
                    //{
                    //    userDTO.Roles.Add(itemRol.Uro_RolCode ?? string.Empty);
                    //}
                }
                return userDTO;
            }
        }

        private void GetReport(string reportCode)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                adReportTypes resultreport = bd.adReportTypes.FirstOrDefault(p => p.RptCode == reportCode);
                if (resultreport != null)
                    report = resultreport;
            }
        }

        private adReportServers GetReportServers(string rptReportServer_RpsCode)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                return bd.adReportServers.FirstOrDefault(x => x.RpsCode == rptReportServer_RpsCode);
            }
        }

        private string CanBePrinted(string reportCode, List<string> userRoles, string userCode, int sourceId,
                                    string sourceCode)
        {
            var message = new StringBuilder();

            // 1. Check if active
            if (!(report.RptActive != null && report.RptActive == Operation.True))
            {
                message.AppendLine(String.Format(Messages.warning_InactiveReport, report.RptCode));
            }

            // 2. Check if print is allowed
            if (!(report.RptPrintAllowed != null && report.RptPrintAllowed == Operation.True))
            {
                message.AppendLine(String.Format(Messages.Not_allowed_to_print, report.RptCode));
            }

            // 3. Check if Max allowed copies vs history
            if (report.RptAllowedCopies > 0)
            {
                if (!GetHistoryCopies(reportCode, sourceId, sourceCode, report.RptAllowedCopies))
                {
                    message.AppendLine(String.Format(Messages.warning_MaxReportCopiesReached, report.RptCode,
                                                     report.RptAllowedCopies));
                }
            }

            // 4. Check role access
            if ((report.RptCheckRoleAccess != null && report.RptCheckRoleAccess == Operation.True) &&
                !RolesHasPermission(reportCode, userRoles))
            {
                message.AppendLine(String.Format(Messages.warning_UserHasNoPermissionToPrintReport, userCode, report.RptCode));
            }

            return message.ToString().Length > 0 ? message.ToString() : string.Empty;
        }

        private bool RolesHasPermission(string reportCode, List<string> userRoles)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                List<adReportRoles> ListadReportRoles2 = bd.adReportRoles.Where(x => userRoles.Contains(x.RerRole_RolCode)).ToList();

                List<adReportRoles> listItemExist = ListadReportRoles2.Where(x => x.RerReportType_RptCode == reportCode).ToList();
                if (listItemExist.Any())
                    return true;
            }
            return false;
        }

        private adPrinters GetReportPrinter(string subsidiary, string userCode, List<string> userRoles, string idReportcode)
        {


			var util = new Utility();
			idReportcode = util.GetReportCodeBySubCode(subsidiary, idReportcode);
			using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                IQueryable<adReportPrintSettings> listadReportPrintSettings = bd.adReportPrintSettings.Where(x => x.RpsReport_RptCode == report.RptCode);

                //1. Check if there is a ReportSetting for report
                if (listadReportPrintSettings.Any())
                {
                    adReportPrintSettings reportPrintSetting;

                    

                    if (subsidiary != null && userRoles != null && userRoles.Any() && userCode != null)
                    {
                        reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsSubsidiary_SubCode == subsidiary && userRoles.Contains(x.RpsRol_RolCode) && x.RpsUser_UsrCode != null && x.RpsUser_UsrCode == userCode && x.RpsReport_RptCode == idReportcode);
                        if (reportPrintSetting != null)
                        {
                            string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                            return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                        }
                    }

                    if (subsidiary != null && userCode != null)
                    {
                        reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsSubsidiary_SubCode == subsidiary && x.RpsUser_UsrCode != null && x.RpsUser_UsrCode == userCode && x.RpsReport_RptCode == idReportcode);
                        if (reportPrintSetting != null)
                        {
                            string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                            return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                        }
                    }

                    //if (subsidiary != null && userRoles != null && userRoles.Any() && userCode == null)
                    //{
                    //    reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsSubsidiary_SubCode == subsidiary && userRoles.Contains(x.RpsRol_RolCode) && x.RpsReport_RptCode == idReportcode);
                    //    if (reportPrintSetting != null)
                    //    {
                    //        string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                    //        return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                    //    }
                    //}


                    if (subsidiary != null)
                    {
                      ///ojo
                      reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsSubsidiary_SubCode == subsidiary && x.RpsReport_RptCode == idReportcode && string.IsNullOrEmpty(x.RpsPro_ProCode) && string.IsNullOrEmpty(x.RpsBop_BopCode) && string.IsNullOrEmpty(x.RpsUser_UsrCode));
                        if (reportPrintSetting != null)
                        {
                            string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                            return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                        }
                    }
                    else
                    {
                      reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsSubsidiary_SubCode == subsidiary && x.RpsReport_RptCode == idReportcode);
                      if (reportPrintSetting != null)
                      {
                        string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                        return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                      }
                    }


                    //if (company != null)
                    //{
                    //    reportPrintSetting = bd.adReportPrintSettings.FirstOrDefault(x => x.RpsCompany_ComCode == company && x.RpsReport_RptCode == idReportcode);
                    //    if (reportPrintSetting != null)
                    //    {
                    //        string pintcode = reportPrintSetting.RpsPrinter_PriCode;
                    //        return bd.adPrinters.FirstOrDefault(x => x.PriCode == pintcode);
                    //    }
                    //}

                }
                return null;
            }
        }

        private bool GetHistoryCopies(string reportCode, int sourceId, string sourceCode, int copyNumber)
        {
            using (var bd = new SPX_SUP_PROD_ReportsEntities())
            {
                List<adReportHistory> listadReportRoles2 =
                  bd.adReportHistory.Where(
                    x => x.RphReportType_RptCode == reportCode && x.RphSourceId == sourceId && x.RphSourceCode == sourceCode).
                    ToList();
                if (listadReportRoles2.Any())
                {
                    if (listadReportRoles2.Count >= copyNumber)
                        return false;
                }
            }
            return true;
        }

        public NetworkCredential GetServiceCredential(adReportServers reportServers)
        {
            if (!string.IsNullOrEmpty(reportServers.RpsServiceUser) && !string.IsNullOrEmpty(reportServers.RpsServicePassword))
            {
                return new NetworkCredential(userName: reportServers.RpsServiceUser, domain: reportServers.RpsServiceDomain,
                                             password: reportServers.RpsServicePassword);
            }
            return null;
        }


        #endregion
    }
}