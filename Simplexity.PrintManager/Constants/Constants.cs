﻿namespace Simplexity.PrintManager.BusinessRules.Constants
{

    public class MessagesConstants
    {
        public enum Types
        {
            None = 0,
            Information = 1,
            Warning = 2,
            Error = 3
        }
    }

    public static class Operation
    {
        public static string True
        {
            get { return "T"; }
        }

        public static string False
        {
            get { return "F"; }
        }
    }
}
