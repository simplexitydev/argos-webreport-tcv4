﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplexity.PrintManager.BusinessRules.DTOs
{
    public class UserDTO
    {
        public string Code { get; set; }
        public string Company { get; set; }
        public string Subsidiary { get; set; }
        public List<string> Roles { get; set; }
    }
}
