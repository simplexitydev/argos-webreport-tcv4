﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Simplexity.PrintManager.BusinessRules.Constants;

namespace Simplexity.PrintManager.BusinessRules.DTOs
{
    public class MessageDTO
    {
        public MessageDTO()
        {
            TypeEnum = MessagesConstants.Types.Information;
        }

        [XmlIgnore]
        [DataMember]
        public MessagesConstants.Types TypeEnum { set; get; }

        [DataMember]
        public int Type
        {
            get { return (int)TypeEnum; }
            private set { }
        }
        //
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string TransactionNumber { get; set; }
    }
}