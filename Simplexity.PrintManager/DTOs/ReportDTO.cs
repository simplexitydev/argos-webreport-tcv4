﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Simplexity.PrintManager.BusinessRules.DTOs
{

    [DataContract]
    public class ReportDTO
    {
      private List<ParamDTO> _params;

      [DataMember]
        public string ReportCode { get; set; }
        [DataMember]
        public int SourceId { get; set; }
        [DataMember]
        public string SourceCode { get; set; }
        [DataMember]
        public List<ParamDTO> Params
        {
          get { return _params ?? (new List<ParamDTO>()); }
          set { _params = value; }
        }
    }
}
