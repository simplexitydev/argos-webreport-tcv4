﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplexity.PrintManager.BusinessRules.DTOs
{
    public class ParamDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
