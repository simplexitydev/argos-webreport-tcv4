﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Crosscutting.EF;

namespace Simplexity.PrintManager.BusinessRules
{
    public class Utility
    {
        public string CheckIfSubsidiaryHasAutoPrint(string sudCode)
        {
            //var result = string.Empty;
            //using (var bd = new SPX_SUP_PROD_ReportsEntities())
            //{
            //    var sudsidiary = bd.adSubsidiaries.FirstOrDefault(x => x.SubCode == sudCode);
            //    if (sudsidiary != null)
            //    {
            //        if (!string.IsNullOrEmpty(sudsidiary.SubAutomaticPrint))
            //        {
            //            if (sudsidiary.SubAutomaticPrint == "T" || sudsidiary.SubAutomaticPrint == "t" || sudsidiary.SubAutomaticPrint == "F" || sudsidiary.SubAutomaticPrint == "f")
            //            {
            //                result = sudsidiary.SubAutomaticPrint;
            //            }
            //        }
            //    }
            //}
            //return result;
          var result = string.Empty;
          using (var connection =
        new SqlConnection(
          System.Configuration.ConfigurationManager.ConnectionStrings["MasterDBAdo"].ToString()))
          {
            //var sessionCode = "b5a0a788644d411cf94d70ece40bb67a";
            SqlCommand command = new SqlCommand(
              "SELECT SubAutomaticPrint FROM adSubsidiaries where SubCode = '" + sudCode + "';",
              connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
              while (reader.Read())
              {
                //Console.WriteLine("{0}\t{1}", reader.GetInt32(0),
                //    reader.GetString(1));
                result = reader.GetString(0);
                if (result == "T" || result == "t" || result == "F" || result == "f")
                {
                  
                }
                else
                {
                  result = string.Empty;
                }
              }
            }
            else
            {
              //Console.WriteLine("No rows found.");
            }
            reader.Close();
          }

          return result;
        }

        public string GetUserbySessionCookies(string sessionCode)
        {
            //using (var bd = new SPX_SUP_PROD_ReportsEntities())
            //{
            //    var userCodeComplete = (bd.adSessions.Where(p => p.SesCode == sessionCode)).FirstOrDefault();
            //    if (userCodeComplete != null)
            //    {
            //        return !string.IsNullOrEmpty(userCodeComplete.Ses_UsrCode) ? userCodeComplete.Ses_UsrCode : string.Empty;
            //    }
            //}
            //return string.Empty;
          var result = string.Empty;
          using (var connection =
        new SqlConnection(
          System.Configuration.ConfigurationManager.ConnectionStrings["MasterDBAdo"].ToString()))
          {
            //var sessionCode = "b5a0a788644d411cf94d70ece40bb67a";
            SqlCommand command = new SqlCommand(
              "SELECT Ses_UsrCode FROM adSessions where SesCode = '" + sessionCode + "';",
              connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
              while (reader.Read())
              {
                //Console.WriteLine("{0}\t{1}", reader.GetInt32(0),
                //    reader.GetString(1));
                result = reader.GetString(0);
              }
            }
            else
            {
              //Console.WriteLine("No rows found.");
            }
            reader.Close();
          }

          return result;
        }

        public string GetSubsidiarybySessionCookies(string sessionCode)
        {
            //using (var bd = new SPX_SUP_PROD_ReportsEntities())
            //{
            //    var userCodeComplete = (bd.adSessions.Where(p => p.SesCode == sessionCode)).FirstOrDefault();
            //    if (userCodeComplete != null)
            //    {
            //        return !string.IsNullOrEmpty(userCodeComplete.Ses_SubCode) ? userCodeComplete.Ses_SubCode : string.Empty;
            //    }
            //}
          var result = string.Empty;
          using (var connection =
        new SqlConnection(
          System.Configuration.ConfigurationManager.ConnectionStrings["MasterDBAdo"].ToString()))
          {
            //var sessionCode = "b5a0a788644d411cf94d70ece40bb67a";
            SqlCommand command = new SqlCommand(
              "SELECT Ses_SubCode FROM adSessions where SesCode = '" + sessionCode + "';",
              connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
              while (reader.Read())
              {
                //Console.WriteLine("{0}\t{1}", reader.GetInt32(0),
                //    reader.GetString(1));
                result = reader.GetString(0);
              }
            }
            else
            {
              //Console.WriteLine("No rows found.");
            }
            reader.Close();
          }

            return result;
        }

        public string GetRpsPathBySubCode(string subCode)
        {
            var result = string.Empty;
            using (var connection =
            new SqlConnection(
            System.Configuration.ConfigurationManager.ConnectionStrings["MasterDBAdo"].ToString()))
            {
                SqlCommand command = new SqlCommand(
                  "SELECT TOP 1 RpsPath FROM adSubsidiaries JOIN AdReportServers ON Rps_Socid = Sub_SocId WHERE SubCode='" + subCode + "' AND RpsCode LIKE 'ArgosServerPrint%'",
                  connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //Console.WriteLine("{0}\t{1}", reader.GetInt32(0),
                        //    reader.GetString(1));
                        result = reader.GetString(0);
                    }
                }
                else
                {
                    //Console.WriteLine("No rows found.");
                }
                reader.Close();
            }

            if (string.IsNullOrEmpty(result))
            {
                result = System.Configuration.ConfigurationManager.AppSettings["ReportFolder"];
            }
            return result;
        }

		public string GetReportCodeBySubCode(string subCode, string rptCode )
		{
			var result = string.Empty;
			using (var connection =
			new SqlConnection(
			System.Configuration.ConfigurationManager.ConnectionStrings["MasterDBAdo"].ToString()))
			{
				SqlCommand command = new SqlCommand(
				  "select RptCode from adReportTypes where RptReportName='" + rptCode + "' and Rpt_Socid = (select Sub_SocId from adSubsidiaries where SubCode='" + subCode + "')",
				  connection);
				connection.Open();
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						//Console.WriteLine("{0}\t{1}", reader.GetInt32(0),
						//    reader.GetString(1));
						result = reader.GetString(0);
					}
				}
				else
				{
					//Console.WriteLine("No rows found.");
				}
				reader.Close();
			}

			if (string.IsNullOrEmpty(result))
			{
				result = System.Configuration.ConfigurationManager.AppSettings["ReportFolder"];
			}
			return result;
		}

    }
}
