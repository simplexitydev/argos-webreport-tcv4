﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Simplexity.TC.TMS.Report.ReportingService.TC
{
    public partial class TestTCReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string urlvalues = "http://localhost:52345/ReportView.aspx?report=rptLoadingOrder&SourceId=0&SourceCode=VRC-00004126&UserCode=spxer&settings=userCode=spxer|Preview=True|reportValue=VRC-00004126";
            //string urlvalues = "http://localhost:52345/ReportView.aspx?report=rptShipmentDoc&SourceId=0&SourceCode=RRC-00000597&UserCode=spxer&settings=userCode=spxer|Preview=True|reportValue=RRC-00000597";
            //string urlvalues = "http://localhost:52345/ReportView.aspx?report=rptR003Receive&IsReport=T";   //&SourceId=0&SourceCode=RRC-00000204&UserCode=spxfr&settings=reportValue=RRC-00000204|Preview=False|userCode=spxjs";

            //string urlvalues = "http://localhost:52345/ReportView.aspx?report=rptScaleTicket&SourceId=0&SourceCode=MPRC-00000243&UserCode=&settings=reportValue=MPRC-00000243|userCode=spxer|Preview=False";

            //string urlvalues = "http://localhost:52345/ReportView.aspx?ITF=default&report=rptR012ShipmentNoRemision&IsReport=T";

            //string urlvalues = "http://localhost:52345/ReportView.aspx?report=rptShipmentDoc&SourceId=0&SourceCode=RCDM-00000018&UserCode=spxer&settings=userCode=spxer|reportValue=RCDM-00000018|Preview=False";


			//string urlvalues = "http://localhost:52345/ReportView.aspx?ReportView.aspx?report=rptShipmentDoc&SourceId=0&SourceCode=RVPDO-00000094&UserCode=kgomezgo&settings=userCode=kgomezgo|reportValue=RVPDO-00000094|Preview=False";

            //string urlvalues = "http://localhost:52345/ReportView.aspx?ITF=default&report=rptR012ShipmentNoRemision&IsReport=T&sessionId=26f0abcd0b944041b96acea7b0037896";

            string urlvalues = "http://localhost:52345/ReportView.aspx?ITF=default&report=rptR010OrdersPendingDispatch&IsReport=T&sessionId=26f0abcd0b944041b96acea7b0037896";

            Response.Redirect(urlvalues);
        }

    }
}