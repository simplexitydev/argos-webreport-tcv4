﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportView.aspx.cs" Inherits="Simplexity.TC.TMS.Report.ReportingService.TC.ReportView" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
  <%--  <script language='JavaScript' src='Scripts/jquery-2.0.3.js' type='text/javascript'></script>--%>
  <%--<script src='Scripts/jquery-2.0.3.intellisense.js' type='text/javascript'></script>
  <script src='Scripts/jquery-2.0.3.js' type='text/javascript'></script>--%>
  <%--<script src='Scripts/jquery.alerts.js' type='text/javascript'></script>--%>
  <script src='Scripts/json2.js' type='text/javascript'></script>

  <link rel="stylesheet" href='Styles/jquery.alerts.css' type='text/css' />
</head>


<body>
  <form runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
      <ContentTemplate>
        <fieldset>
          <div style="left: 63px; background-color: #2F5983; margin-left: 10px; margin-bottom: 20px; margin-right: 10px; height: 31px;">
            &nbsp;&nbsp;&nbsp;
              <asp:Button ID="btnPrint" Text="      Imprimir" runat="server" Height="31px" ToolTip="Comprueba si tiene autorización para enviar a la impresora y realiza el envío" Width="85px" BorderStyle="Dotted " Style="background: url(img/printIcon.png) left no-repeat" ForeColor="White" OnClick="btnPrint_Click" />
            <div>
              <asp:Label runat="server" ID="lblNotification" Style="padding: 1px; margin: 5px; font-size:small" ForeColor="Red"></asp:Label>
            </div>
          </div>
        </fieldset>
      </ContentTemplate>
    </asp:UpdatePanel>

    <div class="divreportThin" id="reportDiv" runat="server" >

      <rsweb:ReportViewer ID="rViewer" runat="server" Height="100%"
        ProcessingMode="Remote" Width="100%" >
      </rsweb:ReportViewer>
    </div>

  </form>
  <div style="left: 63px; background-color: #2F5983; margin-left: 10px; margin-top: 20px; margin-right: 10px; height: 31px;" />
</body>
</html>
