﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Reporting.WebForms;
using Simplexity.PrintManager.BusinessRules;
using Simplexity.PrintManager.BusinessRules.Constants;
using Simplexity.PrintManager.BusinessRules.DTOs;

namespace Simplexity.TC.TMS.Report.ReportingService.TC
{
    public partial class ReportView : Page
    {
        public string IsReport
        {
            get { return ViewState["IsReport"] != null ? ViewState["IsReport"].ToString() : "F"; }
            set { ViewState["IsReport"] = value.ToLower(); }
        }
        public string ReportCode
        {
            get { return ViewState["ReportCode"].ToString(); }
            set { ViewState["ReportCode"] = value.ToLower(); }
        }
        public string SourceId
        {
            get
            {
                int result;
                if (int.TryParse(ViewState["SourceId"].ToString(), out result))
                {
                    return result.ToString();
                }

                return "0";
            }
            set { ViewState["SourceId"] = value; }
        }
        public string SourceCode
        {
            get { return ViewState["SourceCode"].ToString(); }
            set { ViewState["SourceCode"] = value; }
        }
        public string UserCode
        {
            get { return ViewState["UserCode"].ToString(); }
            set { ViewState["UserCode"] = value; }
        }

        public string subcode
        {
            get { return ViewState["subcode"].ToString(); }
            set { ViewState["subcode"] = value; }
        }

        private string _report;

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-CO");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-CO");
            var reportPath = "";
            try
            {
              var sessionCode = string.Empty;
                if (IsPostBack) return;
                if (Request.QueryString.Count <= 0) return;

                IsReport = HttpUtility.UrlDecode(Request.QueryString["IsReport"]) ?? "F";
                //UserCode = HttpUtility.UrlDecode(Request.QueryString["usercode"]) ?? "";
                ReportCode = HttpUtility.UrlDecode(Request.QueryString["report"]) ?? "";
                SourceCode = HttpUtility.UrlDecode(Request.QueryString["sourcecode"]) ?? "";
                SourceId = HttpUtility.UrlDecode(Request.QueryString["sourceid"]) ?? "";
                sessionCode = HttpUtility.UrlDecode(Request.QueryString["sessionId"]) ?? "";

                //GetUserCode and subcode
                var current = HttpContext.Current;
                if (current.Session != null)
                {
                    //if (Request.Cookies["Session"] == null || string.IsNullOrEmpty(Request.Cookies["Session"].Value))
                    //{
                    //    lblNotification.Text = "La session de la Cookies no esta activa o es nula ";
                    //    btnPrint.Visible = false;
                    //    rViewer.ShowPrintButton = false;
                    //    return;
                    //}
                    //sessionCode = Request.Cookies["Session"].Value;
                    //sessionCode = "e16e4e4c738043bf96246767fe0d273d";
                    var util = new Utility();
                    subcode = util.GetSubsidiarybySessionCookies(sessionCode);
                    UserCode = util.GetUserbySessionCookies(sessionCode);
                    var res = util.CheckIfSubsidiaryHasAutoPrint(subcode);
                    reportPath = util.GetRpsPathBySubCode(subcode);
                    
                    //lblNotification.Text = subcode + "-" + UserCode;
                    //subcode = "D026";
                    //UserCode = "jpinzonp";
                    //var res = "T";

                    if (!string.IsNullOrEmpty(res))
                    {
                        if (res == "F" || res == "f")
                        {
                            rViewer.ShowPrintButton = true;
                            btnPrint.Visible = false;
                        }
                        else
                        {
                            if (res == "T" || res == "t")
                            {
                                btnPrint.Visible = true;
                                rViewer.ShowPrintButton = false;
                            }
                        }
                    }
                    else
                    {
                        btnPrint.Visible = true;
                        rViewer.ShowPrintButton = true;
                    }
                }
                else
                {
                    lblNotification.Text = "Session cookies no esta activa o es nula ";
                    btnPrint.Visible = false;
                    rViewer.ShowPrintButton = false;
                }

                //_report = ConfigurationManager.AppSettings["ReportFolder"] + Request.QueryString["report"];
                if (string.IsNullOrEmpty(reportPath))
                {
                    _report = ConfigurationManager.AppSettings["ReportFolder"] + Request.QueryString["report"];
                }else
                {
                    _report = "/" + reportPath + "/" + Request.QueryString["report"];
                }
                
                //Cargar parametros

                var reportUser = ConfigurationManager.AppSettings["ReportUser"];
                var reportDomain = ConfigurationManager.AppSettings["ReportDomain"];
                var reportPass = ConfigurationManager.AppSettings["ReportPass"];

                rViewer.Height = 600;
                rViewer.ProcessingMode = ProcessingMode.Remote;
                rViewer.ServerReport.ReportServerCredentials = new CustomReportCredentials(reportUser, reportPass, reportDomain);
                rViewer.ShowCredentialPrompts = true;
                rViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServer"]);
                rViewer.ServerReport.ReportPath = _report;

                if (IsReport == "F" || IsReport == "f") //Si es F se imprime formato, si es T se imprime un reporte de listados
                {
                    var settings = HttpUtility.UrlDecode(Request.QueryString["settings"]);
                    var parameters = GetParemeters(settings);
                    if (parameters != null)
                    {
                        rViewer.ShowParameterPrompts = false;
                        rViewer.ServerReport.SetParameters(parameters);
                    }
                }
                else
                {
                  //inlcuir la session para los listados

                  if (!string.IsNullOrEmpty(sessionCode))
                  {
                    var parameters = new ReportParameter[1];
                    parameters[0] = new ReportParameter("SessionId", sessionCode);
                    rViewer.ShowParameterPrompts = false;
                    rViewer.ServerReport.SetParameters(parameters);
                  }

                  //fin inlcuir la session
                    rViewer.ShowParameterPrompts = true;
                    reportDiv.Style.Add("width", "auto  !important");
                    reportDiv.Style.Add("height", "auto  !important");
                    reportDiv.Style.Add("border", "0px  !important");
                }

                rViewer.ServerReport.Refresh();
                lblNotification.ForeColor = System.Drawing.Color.Blue;

            }
            catch (Exception ex)
            {
                lblNotification.Text = "Error en el proceso de cargar el informe, revise la conexión a la red y los parámetros enviados";
                HandleException(ex);
            }
        }

        private IEnumerable<ReportParameter> GetParemeters(string settings)
        {
            try
            {
                var parameters = settings.Split('|');
                var result = new ReportParameter[parameters.Count()];
                for (var i = 0; i < parameters.Count(); i++)
                {
                    var values = parameters[i].Split('=');
                    result[i] = new ReportParameter(values[0], values[1]);
                }
                return result;
            }
            catch (Exception)
            {
                lblNotification.Text = "Error en la carga de los parametros";
                return null;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                var util = new Utility();
                var reportDTO = new ReportDTO();
                reportDTO.ReportCode = ReportCode;
                if (SourceId != null) reportDTO.SourceId = Convert.ToInt32(SourceId);
                reportDTO.SourceCode = SourceCode;
                reportDTO.Params = new List<ParamDTO>();
                var parameters = HttpUtility.UrlDecode(Request.QueryString["settings"]).Split('|');
                for (var i = 0; i < parameters.Count(); i++)
                {
                    var values = parameters[i].Split('=');

                    var paramDTO = new ParamDTO();
                    paramDTO.Name = values[0];
                    paramDTO.Value = values[1];
                    reportDTO.Params.Add(paramDTO);
                }
                var messageDTO = new MessageDTO();
                var printManager = new PrintManager.BusinessRules.PrintManager();
                if (reportDTO != null && UserCode != null)
                {
                 
                    string path = util.GetRpsPathBySubCode(subcode);
                    messageDTO = printManager.PrintReport(reportDTO, UserCode, subcode, path);


                    if (messageDTO.TypeEnum != MessagesConstants.Types.Error)
                    {
                        lblNotification.ForeColor = System.Drawing.Color.Blue;
                        lblNotification.Text = "Mensaje: " + messageDTO.Message.Trim();
                    }
                    else
                    {
                        HandleException(new Exception(messageDTO.Message));
                        lblNotification.ForeColor = System.Drawing.Color.Red;
                        lblNotification.Text = "Error: " + messageDTO.Message;
                        //"Error al intentar imprimir con la configuración siguiente: Usuario:" + UserCode + ", Centro: " + subcode + ", Compañia: " + printManager.GetCompany(subcode);
                    }
                }
                else
                {
                    lblNotification.Text = "Los datos recibidos de usuario o reporte no pueden ser nulos";
                }
            }
            catch (Exception ex)
            {
                string ParamData = " / ";
                try
                {
                    var parameters = HttpUtility.UrlDecode(Request.QueryString["settings"]).Split('|');
                    for (var i = 0; i < parameters.Count(); i++)
                    {
                        var values = parameters[i].Split('=');

                        ParamData = ParamData + values[0] + ": " + values[1] + " / " ;
                    }
                }
                catch { }
                
                var exception = new SystemException(ParamData +"\n" + " ReportCode: " + ReportCode + ", UserCode: " + UserCode + ", subcode: " + subcode   + ", Exception envida por el sistema del Systema: ", ex);

                HandleException(exception);
                lblNotification.ForeColor = System.Drawing.Color.Red;
                lblNotification.Text = "Error al intentar imprimir con la configuración siguiente: Usuario:" + UserCode + ", Centro: " + subcode;
                rViewer.ServerReport.Refresh();
            }
        }

        protected MessageDTO HandleException(Exception ex)
        {
            var message = new MessageDTO { TypeEnum = MessagesConstants.Types.Error };

            IConfigurationSource config = ConfigurationSourceFactory.Create();

            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(), false);

            var logWriterFactory = new LogWriterFactory(config);
            Logger.SetLogWriter(logWriterFactory.Create(), false);

            var exceptionPolicyFactory = new ExceptionPolicyFactory(config);
            ExceptionManager exManager = exceptionPolicyFactory.CreateManager();

            ExceptionPolicy.SetExceptionManager(exManager, false);

            Exception newException;
            bool booResult = exManager.HandleException(ex, "All Exceptions Policy", out newException);
            if (!booResult)
            {
                message.Message = "Error cannot be handled";
                return message;
            }

            message.Message = newException.Message;
            message.TypeEnum = MessagesConstants.Types.Error;

            return message;
        }

    }
}