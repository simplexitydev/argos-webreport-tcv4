﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
//using Infrastructure.Crosscutting.Reports;
using Infrastructure.Crosscutting.Reports;
using Infrastructure.Crosscutting.ssrsExecutionService;

namespace Infrastructure.Crosscutting
{
    public class PrintWithReportingService
    {
        //Stream's to hold the pages of report.
        private IList<Stream> m_streams;

        //To hold the current page index when we iterate through the pages.
        private int m_currentPageIndex;

        //Change to point to your report.
        public string ReportPath = string.Empty;
        public string printerName = string.Empty;
        public string serviceReportServer = string.Empty;
        //public string urlprint = string.Empty;

        [STAThread]
        public string PrintToPrint(string reportPath, List<Parameter> ssrsParameters, string printeractive, NetworkCredential userCrediential, string serviceReportServer1)
        {
            //PrintReports pr = new PrintReports();
            //pr.Run();
            //Console.Read();
            ReportPath = reportPath;
            printerName = printeractive;
            serviceReportServer = serviceReportServer1;
            return Run(reportPath, ssrsParameters, printeractive, userCrediential);

        }

        public string Run(string reportPath, List<Parameter> ssrsParameters, string printeractive, NetworkCredential userCrediential)
        {
            //First render the report and get each pages as streams.
            var getCountPage = ExportReportToEMF(ssrsParameters, userCrediential);
            if (getCountPage != string.Empty)
            {
                return "Error al obtener el número de Páginas del reporte..., " + getCountPage;
            }
            //Set the current page to 0.
            m_currentPageIndex = 0;

            //Send the streams to printer for printing.
            string sendToPrint = PrintReportToPrinter();
            if (sendToPrint != string.Empty)
            {
                return "Error al enviar a la impresora..., " + getCountPage;
            }

            return string.Empty;
        }

        /// <summary>
        /// Fetches the stream one by one.
        /// Creates the metafile for the printer.
        /// Draws it to the printers buffer for the 
        /// printer to process the pages.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new Metafile(m_streams[m_currentPageIndex]);
            ev.Graphics.DrawImage(pageImage, ev.PageBounds);
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        /// <summary>
        /// Setting the printer properties with the printer you want to print.
        /// </summary>
        private string PrintReportToPrinter()
        {
            //if (urlprint != string.Empty)
            //    return urlprint;
            if (m_streams == null || m_streams.Count == 0)
                return "Stream's to hold the pages of report..., Error al calcular el número de páginas del reporte";

            var printDoc = new PrintDocument();

            //printerName = "HP Deskjet 3520 series (Red)";
            printDoc.PrinterSettings.PrinterName = printerName;
            if (!printDoc.PrinterSettings.IsValid)
            {
                string msg = String.Format("No se pudo encontrar la impresora:  \"{0}\".", printerName);
                //Console.WriteLine(msg);
                return msg;
            }
            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            printDoc.Print();

            return string.Empty;
        }

        /// <summary>
        /// URL access rendering of report to EMF format 
        /// with PersisStreams. 
        /// This code works is specifically created keeping R2 in mind.
        /// </summary>
        public string ExportReportToEMF(List<Parameter> ssrsParameters, NetworkCredential userCrediential1)
        {
            string res = string.Empty;
            try
            {
                m_streams = new List<Stream>();
                ExecutionHeader execHeader = new ExecutionHeader();
                string uRrlparamaters = string.Empty;
                const string persistStreams = "&rs:PersistStreams=true";
                const string getNextStream = "&rs:GetNextStream=true";
                string sessionID = string.Empty;

                foreach (var itemPar in ssrsParameters)
                {
                    uRrlparamaters = uRrlparamaters + "&" + itemPar.Name + "=" + itemPar.Value;
                }

                NetworkCredential userCrediential = userCrediential1;

                //Create a proxy object for ReportExecution2005.asmx referenced in the project.
                var report = new Infrastructure.Crosscutting.ssrsExecutionService.ReportExecutionService();
                report.Credentials = userCrediential;

                report.Url = serviceReportServer;
                report.ExecutionHeaderValue = execHeader;

                //report.SetExecutionParameters()
                //Build the initial request for the reoprt server to render the report.
                //string requestUri = string.Format("{0}{1}{2}&rs:Command=Render&rs:Format=IMAGE&rc:OutputFormat=EMF", report.Url, ReportPath, uRrlparamaters);

                string requestUri = string.Format("{0}{1}{2}{3}&rs:Command=Render&rs:Format=IMAGE&rc:OutputFormat=EMF", report.Url, "?", ReportPath, uRrlparamaters);

                Console.WriteLine(requestUri);
                //&rs:Command=Render&rs:Format=IMAGE&rc:OutputFormat=EMF
                //Create a HTTP request which notifies the server to process the page 1 and
                // keep rendering the rest of the pages in the server.


                CookieContainer cookies = new CookieContainer();
                WebRequest request = WebRequest.Create(requestUri + persistStreams);
                (request as HttpWebRequest).CookieContainer = cookies;

                //Variable to check if we've reached the end of pages.
                long streamlength = 0;


                //Breaks from the loop only when we read an empty stream marking the end of pages.
                while (true)
                {
                    //Authenticates the request with the current logged in windows user credentials.
                    request.UseDefaultCredentials = true;
                    request.Credentials = userCrediential;

                    // Get the corresponding web  response for the request object.
                    WebResponse response = request.GetResponse();


                    //Read the response stream.
                    Stream s = null;
                    MemoryStream ms = new MemoryStream();
                    int count = 0;
                    byte[] data = null;

                    s = response.GetResponseStream();
                    data = new byte[1024];

                    do
                    {
                        count = s.Read(data, 0, data.Length);
                        ms.Write(data, 0, count);
                    } while (count > 0);

                    // Set the memory stream position to beginning so we can read it from the start.
                    ms.Seek(0, SeekOrigin.Begin);

                    if (ms.Length == 0)
                    {
                        break;
                    }
                    //If not end of pages then add the current stream to the list.
                    m_streams.Add(ms);

                    //Request for the subsecuent pages.
                    request = WebRequest.Create(requestUri + getNextStream);
                    (request as HttpWebRequest).CookieContainer = cookies;

                }
            }
            catch (WebException errorCount)
            {
                res = errorCount.ToString();
            }
            return res;
        }
    }
}
