﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Infrastructure.Crosscutting.ssrsExecutionService;
using Simplexity.TC.TMS.Infrastructure.Crosscutting;
using Simplexity.TC.TMS.Infrastructure.Crosscutting.Reports;

namespace Infrastructure.Crosscutting.Reports
{
  public class RSPrintManager : IRSPrintManager
  {
    #region Members

    private MemoryStream _mCurrentPageStream;
    private int _mCurrentPrintingPage;
    private int _mLastPrintingPage;
    private Metafile _mMetafile;
    private byte[][] _mRenderedReport;
    private NetworkCredential _printerCredentials;
    private NetworkCredential _serviceCredentials;
    private SSRSServiceAgent _ssrsServiceAgent;
    private int m_numberOfPages;

    #endregion

    #region Properties

    public NetworkCredential ServiceCredentials
    {
      get { return _serviceCredentials ?? (_serviceCredentials = CredentialCache.DefaultNetworkCredentials); }
      set { _serviceCredentials = value; }
    }

    public NetworkCredential PrinterCredentials
    {
      get { return _printerCredentials ?? (_printerCredentials = CredentialCache.DefaultNetworkCredentials); }
      set { _printerCredentials = value; }
    }

    public string ReportServiceUrl { get; set; }

    #endregion

    #region Ctor

    ///// <summary>
    // /// printer and web service credentials required
    // /// </summary>
    // /// <param name="serviceCredentials"></param>
    // /// <param name="printerCredentials"></param>
    // public RSPrintManager(NetworkCredential serviceCredentials, NetworkCredential printerCredentials)
    // {
    //   if (serviceCredentials == null)
    //   {
    //     throw new ArgumentException("serviceCredentials");
    //   }

    //   if (printerCredentials == null)
    //   {
    //     throw new ArgumentException("printerCredentials");
    //   }

    //   _serviceCredentials = serviceCredentials;
    //   _printerCredentials = printerCredentials;
    // }

    // /// <summary>
    // /// No printer credentials required
    // /// </summary>
    // /// <param name="serviceCredentials"></param>
    // public RSPrintManager(NetworkCredential serviceCredentials)
    // {
    //   if (serviceCredentials == null)
    //   {
    //     throw new ArgumentException("serviceCredentials");
    //   }

    //   _serviceCredentials = serviceCredentials;
    // }

    #endregion

    #region Methods

    public bool PrintSSRSReport(string reportPath, List<Parameter> parameters, int fromPage, int toPage,
                                string printerName)
    {
      if (parameters.Count > 0)
      {
        ParameterValue[] ssrsReportParameters =
          parameters.Select(parameter => new ParameterValue {Name = parameter.Name, Value = parameter.Value}).ToArray();

        _ssrsServiceAgent = new SSRSServiceAgent(ssrsReportParameters, _serviceCredentials);
      }
      else
      {
        _ssrsServiceAgent = new SSRSServiceAgent(null, ServiceCredentials);
      }

      _ssrsServiceAgent.ReportServiceUrl = ReportServiceUrl;

      _mRenderedReport = _ssrsServiceAgent.RenderReport(reportPath);

      // Wait for the report to completely render. 
      if (_ssrsServiceAgent.ReportPages < 1)
      {
        return false;
      }

      //Get report pages
      m_numberOfPages = _ssrsServiceAgent.ReportPages;


      var printerSettings = new PrinterSettings
                              {MaximumPage = m_numberOfPages, MinimumPage = 1, PrintRange = PrintRange.SomePages};

      // If page Width is greatrer than 279 mm (a letter page size is 215.9 mm × 279.4 mm) means 
      // page must be printed in landspace (All of this is only true if paper size always is letter)
      if (_ssrsServiceAgent.PageSettings.PaperSize.Width >= 279.4)
      {
        printerSettings.DefaultPageSettings.Landscape = true;
      }

      if (!String.IsNullOrEmpty(printerName))
      {
        printerSettings.PrinterName = printerName;
      }

      var pd = new PrintDocument();

      if (toPage != -1 && fromPage != -1)
      {
        _mCurrentPrintingPage = fromPage;
        _mLastPrintingPage = toPage;
        if (m_numberOfPages < toPage)
        {
          toPage = m_numberOfPages;
          _mLastPrintingPage = toPage;
        }
        if (m_numberOfPages < fromPage)
        {
          fromPage = m_numberOfPages;
          _mCurrentPrintingPage = fromPage;
        }
        printerSettings.FromPage = fromPage;
        printerSettings.ToPage = toPage;
      }
      else
      {
        _mCurrentPrintingPage = 1;
        _mLastPrintingPage = m_numberOfPages;
      }

      if (_printerCredentials != null && !String.IsNullOrEmpty(_printerCredentials.UserName) &&
          !String.IsNullOrEmpty(_printerCredentials.Password))
      {
        // Printing report under an account with enough rights
        using (new Impersonator(_printerCredentials.UserName, _printerCredentials.Domain, _printerCredentials.Password))
        {
          if (!printerSettings.IsValid)
          {
            throw new ArgumentException(String.Format("Printer ({0}) is not available or is not valid",
                                                      printerSettings.PrinterName));
          }

          pd.PrinterSettings = printerSettings;
          pd.PrintPage += pd_PrintPage;

          pd.Print();
        }
      }
      else
      {
        // No impersonalisation requires for print
        if (!printerSettings.IsValid)
        {
          throw new ArgumentException(String.Format("Printer ({0}) is not available or is not valid",
                                                    printerSettings.PrinterName));
        }

        pd.PrinterSettings = printerSettings;
        pd.PrintPage += pd_PrintPage;

        pd.Print();
      }

      return true;
    }

    // Check if a printer is valid
    public bool isValidPrinter(string printerName)
    {
      var printerSettings = new PrinterSettings {PrinterName = printerName};

      if (!String.IsNullOrEmpty(_printerCredentials.UserName) && !String.IsNullOrEmpty(_printerCredentials.Password))
      {
        using (new Impersonator(_printerCredentials.UserName, _printerCredentials.Domain, _printerCredentials.Password))
        {
          return printerSettings.IsValid;
        }
      }
      else
      {
        return printerSettings.IsValid;
      }
    }


    private void pd_PrintPage(object sender, PrintPageEventArgs ev)
    {
      ev.HasMorePages = false;

      if (_mCurrentPrintingPage <= _mLastPrintingPage && MoveToPage())
      {
        // Draw the page   
        ReportDrawPage(ev);

        // If the next page is less than or equal to the last page, 
        // print another page. 
        if (Interlocked.Increment(ref _mCurrentPrintingPage) <= _mLastPrintingPage)
        {
          ev.HasMorePages = true;
        }
      }
    }

    // Method to draw the current emf memory stream 
    private void ReportDrawPage(PrintPageEventArgs ev)
    {
      if (_mCurrentPageStream == null || 0 == _mCurrentPageStream.Length || _mMetafile == null)
      {
        return;
      }
      lock (this)
      {
        ev.Graphics.DrawImage(_mMetafile, ev.PageBounds);
      }
    }

    private bool MoveToPage()
    {
      // Check to make sure that the current page exists in 
      // the array list 
      if (_mRenderedReport[_mCurrentPrintingPage - 1] == null)
      {
        return false;
      }
      // Set current page stream equal to the rendered page 
      _mCurrentPageStream = new MemoryStream(_mRenderedReport[_mCurrentPrintingPage - 1]) {Position = 0};

      // Set its postion to start. 
      // Initialize the metafile 
      if (_mMetafile != null)
      {
        _mMetafile.Dispose();
        _mMetafile = null;
      }
      // Load the metafile image for this page 
      //Alexander Bautista Oct 10 2012: Se comentarió esta línea y se cambió por la siguiente línea. Al parecer 
      //al trabajar con stream si estos contienen imagenes con ciertos tipos por ejemplo PNG necesitam que sea un memorystream 
      // en cambio de un stream
      // reference: http://geeks.ms/blogs/jalarcon/archive/2010/10/18/soluci-243-n-al-mensaje-quot-error-gen-233-rico-de-gdi-quot-en-aplicaciones-web-que-generan-gr-225-ficos.aspx

      //m_metafile = new Metafile((Stream)m_currentPageStream);
      _mMetafile = new Metafile(_mCurrentPageStream);
      return true;
    }

    #endregion
  }
}