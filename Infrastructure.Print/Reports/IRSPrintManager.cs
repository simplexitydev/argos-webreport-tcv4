﻿using System.Collections.Generic;
using System.Net;
using Infrastructure.Crosscutting.Reports;

namespace Simplexity.TC.TMS.Infrastructure.Crosscutting
{
  public interface IRSPrintManager
  {
      bool PrintSSRSReport(string reportPath, List<Parameter> parameters, int fromPage, int toPage, string printerName);
    NetworkCredential ServiceCredentials { get; set; }
    NetworkCredential PrinterCredentials { get; set; }
    string ReportServiceUrl { get; set; }
  }
}
