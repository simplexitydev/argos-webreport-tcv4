﻿using System.Net;
using Infrastructure.Crosscutting.ssrsExecutionService;

namespace Simplexity.TC.TMS.Infrastructure.Crosscutting
{
  public interface ISSRSService
  {
    byte[][] RenderReport(string reportPath);
    string GetDeviceInfo(int pageIndex);
    PageSettings PageSettings { get; }
    int ReportPages { get; }
    ParameterValue[] ReportParameters { get; set; }
    NetworkCredential ServiceCredentials { get; set; }
  }
}
