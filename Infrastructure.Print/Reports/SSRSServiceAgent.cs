﻿using System;
using System.Net;
using System.Text;
using System.Web.Services.Protocols;
using Infrastructure.Crosscutting.ssrsExecutionService;

namespace Simplexity.TC.TMS.Infrastructure.Crosscutting.Reports
{
  public class SSRSServiceAgent : ISSRSService
  {
    #region Members

    private readonly ReportExecutionService _reportExecutionService;
    private int _reportPages;
    private ParameterValue[] _reportParameters;
    private NetworkCredential _serviceCredentials;

    #endregion

    #region Properties

    public int ReportPages
    {
      get { return _reportPages; }
    }

    public ParameterValue[] ReportParameters

    {
      get { return _reportParameters; }
      set { _reportParameters = value; }
    }

    public NetworkCredential ServiceCredentials
    {
      get
      {
        if (_serviceCredentials == null)
          _serviceCredentials = CredentialCache.DefaultNetworkCredentials;

        return _serviceCredentials;
      }
      set { _serviceCredentials = value; }
    }

    public PageSettings PageSettings
    {
      get { return _reportExecutionService.GetExecutionInfo2().ReportPageSettings; }
    }

    public string ReportServiceUrl { get; set; }

    #endregion

    #region Ctor

    public SSRSServiceAgent(ParameterValue[] reportParameters, NetworkCredential serviceCredentials)
    {
      if (reportParameters == null)
      {
        throw new ArgumentException("null parameters");
      }

      if (serviceCredentials == null)
      {
        throw new ArgumentException("null serviceCredentials");
      }

      _reportExecutionService = new ReportExecutionService();
      _serviceCredentials = serviceCredentials;
      _reportParameters = reportParameters;
    }

    public SSRSServiceAgent(NetworkCredential serviceCredentials)
    {
      if (serviceCredentials == null)
      {
        throw new ArgumentException("null credentials");
      }

      _reportExecutionService = new ReportExecutionService();
      _serviceCredentials = serviceCredentials;
    }

    #endregion

    #region ISSRSService Methods

    public byte[][] RenderReport(string reportPath)
    {
      // Private variables for rendering 
      string deviceInfo = null;
      string format = "IMAGE";
      byte[] firstPage = null;
      string encoding = null;
      string mimeType = null;
      Warning[] warnings = null;
      string[] streamIDs = null;
      byte[][] pages = null;
      string historyId = null;
      string extension = null;

      //Exectute the report and get page count. 
      try
      {
        _reportExecutionService.Url = ReportServiceUrl ?? _reportExecutionService.Url;
        _reportExecutionService.Credentials = _serviceCredentials;
        _reportExecutionService.Timeout = 300000;
        _reportExecutionService.ExecutionHeaderValue = new ExecutionHeader();
        _reportExecutionService.LoadReport(reportPath, historyId);

        if ((_reportParameters != null))
        {
          _reportExecutionService.SetExecutionParameters(_reportParameters, "en_us");
        }

        // Build device info based on the start page 
        deviceInfo = GetDeviceInfo(-1);

        // Renders the first page of the report and returns streamIDs for 
        // subsequent pages
        // Tracer para hacer seguimiento.Trace.WriteLine("before render 1st: " + DateTime.Now);

        firstPage = _reportExecutionService.Render(format, deviceInfo, out extension, out mimeType, out encoding,
                                                   out warnings, out streamIDs);

        #region CORRECION PACHO??
        //codigo fixed by pacho
        //var i = 0;
        //pages = new byte[i][];
        //_reportPages = i;
        //while (firstPage.Length != 0)
        //{
        //  i++;
        //  Array.Resize(ref pages, i);
        //  pages[i - 1] = firstPage;
        //  _reportPages = i;
        //  deviceInfo = GetDeviceInfo(i + 1);
        //  firstPage = _reportExecutionService.Render(format, deviceInfo, out extension, out mimeType,
        //                                                    out encoding, out warnings, out streamIDs);
        //}
        #endregion

        // Tracer para hacer seguimiento Trace.WriteLine("after render 1st: " + DateTime.Now);
        //// The total number of pages of the report is 1 + the streamIDs 
        _reportPages = streamIDs.Length + 1;

        pages = new byte[_reportPages][];

        // The first page was already rendered 
        pages[0] = firstPage;
        for (int pageIndex = 1; pageIndex <= _reportPages - 1; pageIndex++)
        {
          // Build device info based on start page 
          deviceInfo = GetDeviceInfo(pageIndex + 1);

          // Tracer para hacer seguimientoTrace.WriteLine("before render last: " + DateTime.Now);
          pages[pageIndex] = _reportExecutionService.Render(format, deviceInfo, out extension, out mimeType,
                                                            out encoding, out warnings, out streamIDs);
          // Tracer para hacer seguimientoTrace.WriteLine("after render last: " + DateTime.Now);
        }
      }
      catch (SoapException ex)
      {
        throw new Exception(ex.Detail.InnerXml, ex);
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message, ex);
      }
      finally
      {
        //Console.WriteLine("Number of pages: {0}", pages.Length);
      }
      return pages;
    }

    public string GetDeviceInfo(int pageIndex)
    {
      var deviceInfo = new StringBuilder();
      string pageWidth = null;
      string pageHeight = null;
      string marginTop = null;
      string marginBottom = null;
      string marginLeft = null;
      string marginRight = null;
      double incheToCm = 0.393700787;

      var exInfo = _reportExecutionService.GetExecutionInfo2();

      pageWidth =
        (Math.Round(exInfo.ReportPageSettings.PaperSize.Width*incheToCm/10, 2)).ToString().Replace(",", ".") + "in";
      pageHeight =
        (Math.Round(exInfo.ReportPageSettings.PaperSize.Height*incheToCm/10, 2)).ToString().Replace(",", ".") + "in";
      marginTop = (Math.Round(exInfo.ReportPageSettings.Margins.Top*incheToCm/10, 2)).ToString().Replace(",", ".") +
                  "in";
      marginBottom =
        (Math.Round(exInfo.ReportPageSettings.Margins.Bottom*incheToCm/10, 2)).ToString().Replace(",", ".") + "in";
      marginLeft = (Math.Round(exInfo.ReportPageSettings.Margins.Left*incheToCm/10, 2)).ToString().Replace(",", ".") +
                   "in";
      marginRight =
        (Math.Round(exInfo.ReportPageSettings.Margins.Right*incheToCm/10, 2)).ToString().Replace(",", ".") + "in";

      deviceInfo.Append(
        String.Format(
          "<DeviceInfo><OutputFormat>{0}</OutputFormat><PageWidth>{1}</PageWidth><PageHeight>{2}</PageHeight>", "EMF",
          pageWidth, pageHeight));
      deviceInfo.Append(
        String.Format(
          "<margintop>{0}</margintop><marginbottom>{1}</marginbottom><marginleft>{2}</marginleft><marginright>{3}</marginright>",
          marginTop, marginBottom, marginLeft, marginRight));

      if (pageIndex != -1)
      {
        deviceInfo.Append(String.Format("<StartPage>{0}</StartPage>", pageIndex));
      }

      deviceInfo.Append("</DeviceInfo>");

      return deviceInfo.ToString();
    }

    #endregion
  }
}