﻿namespace Infrastructure.Crosscutting.Reports
{
    public class Parameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
